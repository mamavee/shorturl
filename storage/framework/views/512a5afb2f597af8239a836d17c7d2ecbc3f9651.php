
<?php if(session('success')): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h1><?php echo e(session('success')); ?></h1>
        <button type="button" class="close" data-dismiss="alert">
            <span>x</span>
        </button>
    </div>
<?php endif; ?>





<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/inc/message.blade.php ENDPATH**/ ?>