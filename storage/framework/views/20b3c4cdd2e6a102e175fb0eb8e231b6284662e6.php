<?php $__env->startSection('content'); ?>

    <form method="post" action="<?php echo e(url('/')); ?>">
        <?php echo csrf_field(); ?>

            <div class="container">
            <br>
            <h1 class="text-center">SHORT URL</h1><br>
                <label>Long URL</label>
                <div class="input-group mb-3">
                    <input type="text" name="long_url" class="form-control" placeholder="Enter the link here"  aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="submit" id="button-addon2">Create Short URL</button>
                    </div>
                </div>
                <br>
                <p class="text-center" >ShortURL.at is a tool to shorten a URL or reduce a link. <br/>Use our URL Shortener to
                    create a shortened<br/> link making it easy to remember.</p>
             </div>
    </form>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/new.blade.php ENDPATH**/ ?>