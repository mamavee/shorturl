<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/short', function () {
//    return view('short');
//});

Route::get('/', function () {
   return view('index');
});

Route::get('/','TodoController@index');
    Route::resource('/','TodoController');

Route::get('/t/{short}', 'Todocontroller@show');
