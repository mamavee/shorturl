{{--index.blade.php--}}
@extends('layouts.app')
@section('content')
    <h1 xmlns="http://www.w3.org/1999/html">List</h1>
    @if(count($todos)>0)
        @foreach($todos as $todo)
            <div>
                <a href="{{url('/todo/'.$todo->id)}}"></a>
                <p>{{$todo->created_at}}</p>

                <a href="{{$todo->long_url}}">
                    <p>{{$todo->long_url}}</p>
                </a>

                <input id="shorturl{{$todo->id}}" class="form-control" type="text"
                       value="http://www.short.local/t/{{$todo->short_url}} " readonly >

                <button onclick="copy(this)" id="btncopy" value="{{$todo->id}}" type="button" class="btn btn-info">Copy</button>

                <p>{{$todo->view}}</p>

            </div>
            <hr>
        @endforeach
    @endif

    <script>
        function copy(clicked) {
            var id = clicked.value;
            var copyText = document.querySelector('#shorturl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('copied ' + copyText.value);  //copyText.value ดึงค่า short url มา
        }
    </script>
@endsection
